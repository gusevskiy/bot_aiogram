# - *- coding: utf-8 - *-
# integer bot
import config
import logging
import sqlite3
from datetime import date
import aiogram
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters import Text

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=config.API_TOKEN)
dp = Dispatcher(bot)

conn = sqlite3.connect('test_db.db', check_same_thread=False)
table_db = "CREATE TABLE IF NOT EXISTS user_data (\
    product BIGINT, \
    things BIGINT, \
    avto BIGINT,\
    house BIGINT,\
    beauty BIGINT,\
    gymnastic BIGINT,\
    other BIGINT);"
conn.execute(table_db)
cursor = conn.cursor()
conn.commit()


def add_in_db(name_column: int, sum: int, date_numb: date):
    cursor.execute(f'INSERT INTO user_data ({name_column}, date) VALUES (?,?)',
                   (sum, date_numb))
    conn.commit()


def get_from_db(get_date):
    cursor.execute(f"select sum(IFNULL(product,0)),"
                   f" sum(IFNULL(things,0)),"
                   f" sum(IFNULL(avto,0)),"
                   f" sum(IFNULL(house,0)),"
                   f" sum(IFNULL(beauty,0)),"
                   f" sum(IFNULL(gymnastic,0)),"
                   f" sum(IFNULL(other,0)),"
                   f" sum(IFNULL(product,0))+sum(IFNULL(things,0))+sum("
                   f"IFNULL(avto,0))+sum(IFNULL(house,0))+sum(IFNULL(beauty,"
                   f"0))+sum(IFNULL(gymnastic,0))+sum(IFNULL(other,0)) "
                   f" from user_data where date >= '{get_date}'")
    a = cursor.fetchall()
    conn.commit()
    for i in a:
        return list(i)


@dp.message_handler()
async def start_handler(message: types.Message):
    adm = [452054525, 5261222287]  # список из id пользователей
    if message.chat.id not in adm:
        await bot.send_message(message.chat.id, 'Мой хозяин вас не знает, '
                                                'мне запрещено с '
                                                'вами разговаривать!!!')
    elif message.text.upper() == 'покажи'.upper():
        keyboard_statistics = types.InlineKeyboardMarkup(row_width=3)
        text_and_data = (
            ('за месяц', 'bottom_month'),
            ('за год', 'bottom_year'),
            ('с 01.05.2022', 'bottom_in_total'),
        )
        row_butn = (types.InlineKeyboardButton(text, callback_data=data) for
                    text, data in text_and_data)
        keyboard_statistics.add(*row_butn)
        await message.reply(f"за какой период?",
                            reply_markup=keyboard_statistics)
    elif message.text.isdigit():
        keyboard_markup = types.InlineKeyboardMarkup(row_width=2)
        text_and_data = (
            ('🍞 еда', 'bottom_product'),
            ('🧦 вещи', 'bottom_things'),
            ('🚗 машина', 'bottom_avto'),
            ('🏠 квартира', 'bottom_house'),
            ('🛁 для красоты', 'bottom_beauty'),
            ('🤸‍ гимнастика', 'bottom_gymnastic'),
            ('🗑 разное', 'bottom_other'),
        )
        row_butn = (types.InlineKeyboardButton(text, callback_data=data) for
                    text, data in text_and_data)
        keyboard_markup.add(*row_butn)
        await message.reply(f"Ну и на что Мы {message.text} рублей потратили?",
                            reply_markup=keyboard_markup)
    else:
        await message.answer('Введи округленную сумму, я твои копейки и '
                             'прописи запоминать не хочу.\n Если хочешь '
                             'посмотреть сколько ты всего потратил напиши '
                             'мне "покажи".')


@dp.callback_query_handler(
    Text(startswith='bottom_'))  # added a filter for bottoms
async def db_message(querty: types.CallbackQuery):
    """ I know that a lot of 'if' is not right,
    but I couldn't think of any other way """
    today_date = date.today()  # Дата внесения заметки
    beginning_month = date.today().replace(day=1)  # показать за текущий месяц
    beginning_year = date.today().replace(month=1,
                                          day=1)  # показать с начала года
    in_total = '2022-04-01'  # за все время (те с начала ведение БД)
    number = querty.data
    await querty.answer(number)

    if number == 'bottom_product':
        text = '🛒 записал что ты много ешь!'
        add_in_db('product', querty.message.reply_to_message.text, today_date)
    elif number == 'bottom_things':
        text = '🧦 записал в шмотье))'
        add_in_db('things', querty.message.reply_to_message.text, today_date)
    elif number == 'bottom_avto':
        text = '🚗 Может быть в следующий раз на велосипеде?'
        add_in_db('avto', querty.message.reply_to_message.text, today_date)
    elif number == 'bottom_other':
        text = 'Опять купили какое то 💩'  # эмодзи
        add_in_db('other', querty.message.reply_to_message.text, today_date)
    elif number == 'bottom_house':
        text = 'Пипец, это уже напрягает,\n ладно записал 😒'
        add_in_db('house', querty.message.reply_to_message.text, today_date)
    elif number == 'bottom_beauty':
        text = 'Записал,\n мыло что ли опять купили?'
        add_in_db('beauty', querty.message.reply_to_message.text, today_date)
    elif number == 'bottom_gymnastic':
        text = 'Записал в гимнастику,\n спорт сила!'
        add_in_db('gymnastic', querty.message.reply_to_message.text,
                  today_date)
    elif number == 'bottom_month':
        text = f'Расходов за этот месяц ​😔​💸​\n' \
               f'Продукты 🛒 - {get_from_db(beginning_month)[0]} руб.\n' \
               f'Тряпки 🧦 - {get_from_db(beginning_month)[1]} руб.\n' \
               f'Машина 🚗 - {get_from_db(beginning_month)[2]} руб.\n' \
               f'Квартира 🏠 - {get_from_db(beginning_month)[3]} руб.\n' \
               f'Для красоты 🛁 - {get_from_db(beginning_month)[4]} руб.\n' \
               f'Гимнастика 🤸 - {get_from_db(beginning_month)[5]} руб.\n' \
               f'Разное 🗑 - {get_from_db(beginning_month)[6]} руб.\n' \
               f'ВСЕГО 😱 - {get_from_db(beginning_month)[7]} руб.'
    elif number == 'bottom_year':
        text = f'Расходов за этот год ​😔​💸​\n' \
               f'Продукты 🛒 - {get_from_db(beginning_year)[0]} руб.\n' \
               f'Тряпки 🧦 - {get_from_db(beginning_year)[1]} руб.\n' \
               f'Машина 🚗 - {get_from_db(beginning_year)[2]} руб.\n' \
               f'Квартира 🏠 - {get_from_db(beginning_year)[3]} руб.\n' \
               f'Для красоты 🛁 - {get_from_db(beginning_year)[4]} руб.\n' \
               f'Гимнастика 🤸 - {get_from_db(beginning_year)[5]} руб.\n' \
               f'Разное 🗑 - {get_from_db(beginning_year)[6]} руб.\n' \
               f'ВСЕГО 😱 - {get_from_db(beginning_year)[7]} руб.'
    elif number == 'bottom_in_total':
        text = f'Всего расходов ​😔​💸​\n' \
               f'Продукты 🛒 - {get_from_db(in_total)[0]} руб.\n' \
               f'Тряпки 🧦 - {get_from_db(in_total)[1]} руб.\n' \
               f'Машина 🚗 - {get_from_db(in_total)[2]} руб.\n' \
               f'Квартира 🏠 - {get_from_db(in_total)[3]} руб.\n' \
               f'Для красоты 🛁 - {get_from_db(in_total)[4]} руб.\n' \
               f'Гимнастика 🤸 - {get_from_db(in_total)[5]} руб.\n' \
               f'Разное 🗑 - {get_from_db(in_total)[6]} руб.\n' \
               f'ВСЕГО 😱 - {get_from_db(in_total)[7]} руб.'
    else:
        text = 'что то сломалось у бота'

    await bot.send_message(querty.from_user.id, text)
    await querty.message.delete()


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
